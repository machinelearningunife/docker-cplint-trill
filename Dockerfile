# Maven - Java 11 image
FROM ubuntu:18.04


RUN apt update -y
RUN apt install -y \
#	openjdk-8-jre \
  openjdk-11-jdk \
	vim \
	git \
  maven \
	software-properties-common

RUN apt-add-repository ppa:swi-prolog/devel
RUN apt update && apt install -y swi-prolog
RUN apt install -y swi-prolog-java


RUN swipl -g "pack_install(cplint, [interactive(false),directory('/root/.local/share/swi-prolog/pack')])."
RUN swipl -g "pack_install(trill, [interactive(false)])."

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

#CMD ["swipl"]

#WORKDIR /bundle

#ADD target/bundle-*-standalone.jar .
#ADD target/classes/bundle .
#ADD examples ./examples

#RUN chmod +x bundle



#ENV PATH="/bundle:${PATH}"
#ENV CLASSPATH="/bundle:${CLASSPATH}"
